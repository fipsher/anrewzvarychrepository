using System;

namespace EpamItLab.Hometasks
{
	class Vector
	{
		private double _x;
		private double _y;
		private double _z;
		
		public Vector()
		{
			_x = 0;
			_y = 0;
			_z = 0;
		}
		
		public Vector(double x, double y, double z)
		{
			_x = x;
			_y = y;
			_z = z;
		}		
		
		public double X
		{
			get
			{
				return _x;
			}
			private set
			{
				_x = value;
			}
		}
		
		public double Y
		{
			get
			{
				return _y;
			}
			private set
			{
				_y = value;
			}
		}
		
		public double Z
		{
			get
			{
				return _z;
			}
			private set
			{
				_z = value;
			}
		}
						
		public double GetVectorLength()
		{
			//return Math.Sqrt(X * X + Y * Y + Z * Z);
			return Math.Sqrt(GetScalarProduct(this));
		}
		
		public double GetAngleBetweenVectors(Vector otherVector)
		{
			double vectorProductLength = GetVectorProduct(otherVector).GetVectorLength();
			return  Math.Asin(vectorProductLength / (GetVectorLength() * otherVector.GetVectorLength()));
		}
		
		public double GetScalarProduct(Vector otherVector)
		{
			//gets ax * bx + ay * by + az * bz
			return X * otherVector.X + Y * otherVector.Y + Z * otherVector.Z;
		}
		
		public double GetTripleProduct(Vector firstVector, Vector secondVector)
		{
			//a * [b x c]
			return GetScalarProduct(firstVector.GetVectorProduct(secondVector));
		}
		
		public Vector GetVectorProduct(Vector otherVector)
		{
			//gets vector that is perpendicular to this two vectors
			double x = Y * otherVector.Z - Z * otherVector.Y;
			double y = Z * otherVector.X - X * otherVector.Z;
			double z = X * otherVector.Y - Y * otherVector.X;
			return new Vector(x, y, z);
		}
		
		public static Vector operator +(Vector first, Vector second)
		{
			return new Vector(first.X + second.X, first.Y + second.Y, first.Z + second.Z);
		}
		
		public static Vector operator -(Vector first, Vector second)
		{
			return first + second * (-1);
		}			
		
		public static Vector operator *(Vector first, double scalar)
		{
			return new Vector(first.X * scalar, first.Y * scalar, first.Z * scalar);
		}
		
		public static Vector operator *(double scalar, Vector first)
		{
			return first * scalar;
		}
		
		public static bool operator ==(Vector first, Vector second)
		{
			if (first.X == second.X && first.Y == second.Y && first.Z == second.Z)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public static bool operator !=(Vector first, Vector second)
		{
			return !(first == second);
		}
				
		public override string ToString()
		{
			return "[" + X + ", " + Y + ", " + Z + "]";
		}
	}
		
	class JustMainClass
	{
		static void Main()
		{
			 Vector vectorA = new Vector(1, 0, 0);
            Console.WriteLine("a = {0}", vectorA);
            Console.WriteLine("a.Length = {0}", vectorA.GetVectorLength());
            Console.WriteLine();

            Vector vectorB = new Vector(0, 1, 0);
            Console.WriteLine("b = {0}", vectorB);
            Console.WriteLine("b.Length = {0}", vectorB.GetVectorLength());
            Console.WriteLine();

            double angle = vectorA.GetAngleBetweenVectors(vectorB);
            Console.WriteLine("angle(a, b) = {0} rad", angle);
            Console.WriteLine();

            Vector sum = vectorA + vectorB;
            Console.WriteLine("c = a + b = {0}", sum.X);
            Console.WriteLine();

            Vector diff = vectorA - vectorB;
            Console.WriteLine("d = a - b = {0}", diff);
            Console.WriteLine();

            Console.WriteLine("a * b = {0}", vectorA.GetScalarProduct(vectorB));
            Console.WriteLine();

            Vector vectProduct = vectorA.GetVectorProduct(vectorB);
            Console.WriteLine("[a x b] = {0}", vectProduct);
            Console.WriteLine();

            double tripleProduct = vectorA.GetTripleProduct(vectorB, sum);
            Console.WriteLine("a * [b x c] = {0}", tripleProduct);
            Console.WriteLine();

            string trueFalseString;
            trueFalseString = vectorA == vectorB ? "true" : "false";
            Console.WriteLine("a == b = {0}", trueFalseString);
						
		}
	}
}