using System;

namespace EpamItLab.Hometasks
{
    class RectMatrix
    {
        private double[,] _matrix;
        private int _width;
        private int _heigth;

        public RectMatrix(int width, int heigth)
        {
            _width = width;
            _heigth = heigth;

            _matrix = new double[_heigth, _width];
        }

        public int Width
        {
            get
            {
                return _width;
            }
            private set
            {
                _width = value;
            }
        }

        public int Heigth
        {
            get
            {
                return _heigth;
            }
            private set
            {
                _heigth = value;
            }
        }

        public RectMatrix Transpose()
        {
            RectMatrix resultMatrix = new RectMatrix(_heigth, _width);

            for (int i = 0; i < Heigth; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    resultMatrix[j, i] = _matrix[i, j];
                }
            }
            return resultMatrix;
        }

        public RectMatrix GetSubMatrix(int heigthPos, int widthPos, int width, int heigth)
        {
            //gets subMatrix from point [heigthPos, widthPos] with Width == width and Heigth == heigth 

            if (Width - widthPos >= width && Heigth - heigthPos >= heigth)
            {
                RectMatrix subMatrix = new RectMatrix(width, heigth);
                for (int i = 0; i < heigth; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        subMatrix[i, j] = _matrix[i + heigthPos, j + widthPos];
                    }
                }
                return subMatrix;
            }
            else
            {
                throw new Exception("Out of range exception");
            }
        }

        public void Show()
        {
            for (int i = 0; i < Heigth; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    Console.Write("{0}\t", _matrix[i, j]);
                }
                Console.WriteLine();
            }
        }

        public double this[int i, int j]
        {
            get
            {
                return _matrix[i, j];
            }

            set
            {
                _matrix[i, j] = value;
            }
        }

        public static RectMatrix operator +(RectMatrix first, RectMatrix second)
        {
            if (first.Width == second.Width && first.Heigth == second.Heigth)
            {
                RectMatrix resultMatrix = new RectMatrix(first.Width, first.Heigth);
                for (int i = 0; i < first.Heigth; i++)
                {
                    for (int j = 0; j < first.Width; j++)
                    {
                        resultMatrix[i, j] = first[i, j] + second[i, j];
                    }
                }
                return resultMatrix;
            }
            else
            {
                throw new Exception("Dimensions must agree");
            }
        }

        public static RectMatrix operator -(RectMatrix first, RectMatrix second)
        {
            return first + second * (-1);
        }

        public static RectMatrix operator *(RectMatrix first, double scalar)
        {
            RectMatrix resultMatrix = new RectMatrix(first.Width, first.Heigth);
            for (int i = 0; i < first.Heigth; i++)
            {
                for (int j = 0; j < first.Width; j++)
                {
                    resultMatrix[i, j] = first[i, j] * scalar;
                }
            }
            return resultMatrix;
        }

        public static RectMatrix operator *(double scalar, RectMatrix first)
        {
            return first * scalar;
        }

        public static bool operator ==(RectMatrix first, RectMatrix second)
        {
            //compare all elements in matrices
            if (first.Width == second.Width && first.Heigth == second.Heigth)
            {
                for (int i = 0; i < first.Heigth; i++)
                {
                    for (int j = 0; j < first.Width; j++)
                    {
                        if (first[i, j] != second[i, j])
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool operator !=(RectMatrix first, RectMatrix second)
        {
            return !(first == second);
        }
    }

    class JustMainClass
    {
        static void Main()
        {
            RectMatrix firstMatrix = new RectMatrix(3, 4);
            Console.WriteLine("A = ");
            for (int i = 0; i < firstMatrix.Heigth; i++)
            {
                for (int j = 0; j < firstMatrix.Width; j++)
                {
                    firstMatrix[i, j] = i + j;
                }
            }
            firstMatrix.Show();
            Console.WriteLine();

            RectMatrix secondMatrix = new RectMatrix(3, 4);
            Console.WriteLine("B = ");
            for (int i = 0; i < secondMatrix.Heigth; i++)
            {
                for (int j = 0; j < secondMatrix.Width; j++)
                {
                    secondMatrix[i, j] = i * j + i;
                }
            }
            secondMatrix.Show();
            Console.WriteLine();

            RectMatrix aPlusBMatrix = new RectMatrix(3, 4);
            Console.WriteLine("A + B = ");
            for (int i = 0; i < aPlusBMatrix.Heigth; i++)
            {
                for (int j = 0; j < aPlusBMatrix.Width; j++)
                {
                    aPlusBMatrix[i, j] = firstMatrix[i, j] + secondMatrix[i, j];
                }
            }
            aPlusBMatrix.Show();
            Console.WriteLine();


            Console.WriteLine("transpose(A) = ");
            firstMatrix.Transpose().Show();
            Console.WriteLine();

			string trueFalseStr = firstMatrix == secondMatrix ? "true" : "false";
			Console.WriteLine("A == B : {0}",trueFalseStr);
			Console.WriteLine();

            Console.WriteLine("Get A subMatrix in point [0, 1] Width = 2, Heigth = 2 ");
            firstMatrix.GetSubMatrix(0, 1, 2, 2).Show();
            Console.WriteLine();

        }
    }
}