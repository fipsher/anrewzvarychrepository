using System;
using System.Collections.Generic;

namespace EpamItLab.Hometasks
{
    class Point
    {
        private double _x;
        private double _y;

		public Point(double x, double y)
        {
            _x = x;
            _y = y;
        }

        public Point(Point point)
        {
            _x = point.X;
            _y = point.Y;
        }

        public double X
        {
            get
            {
                return _x;
            }
            private set
            {
                _x = value;
            }
        }

        public double Y
        {
            get
            {
                return _y;
            }
            private set
            {
                _y = value;
            }
        }

      
        public void SetY(double y)
        {
            _y = y;
        }

        public void SetX(double x)
        {
            _x = x;
        }

        public override string ToString()
        {
            return "[" + _x +", " + _y + "]"; 
        }
    }

    class Rectangle
    {
        private Point _leftBottCornerCoords;
        private double _heigth;
        private double _width;

		public Rectangle()
        {
            _leftBottCornerCoords = new Point(0, 0);
            _heigth = 1;
            _width = 1;
        }

        public Rectangle(Point leftBottCornerCoords, double width, double heigth)
        {
            _leftBottCornerCoords = leftBottCornerCoords;
            _width = width;
            _heigth = heigth;
        }

		
        public Point LeftBottCornerCoords
        {
            get
            {
                return _leftBottCornerCoords;
            }
            private set
            {
                _leftBottCornerCoords = value;
            }
        }

        public double Heigth
        {
            get
            {
                return _heigth;
            }
            private set
            {
                _heigth = value;
            }
        }

        public double Width
        {
            get
            {
                return _width;
            }
            private set
            {
                _width = value;
            }
        }

       
        public Rectangle GetSmallestRectCombinedWithAnotherRect(Rectangle anotherRect)
        {
            double leftSide;
            double rightSide;
            double topSide;
            double bottSide;

			//  finding corners coords
            leftSide = _leftBottCornerCoords.X < anotherRect.LeftBottCornerCoords.X ? _leftBottCornerCoords.X : anotherRect.LeftBottCornerCoords.X;
            rightSide = _leftBottCornerCoords.X + _width > anotherRect.LeftBottCornerCoords.X + anotherRect.Width ? _leftBottCornerCoords.X + _width : anotherRect.LeftBottCornerCoords.X + anotherRect.Width;
            bottSide = _leftBottCornerCoords.Y < anotherRect.LeftBottCornerCoords.Y ? _leftBottCornerCoords.Y : anotherRect.LeftBottCornerCoords.Y;
            topSide = _leftBottCornerCoords.Y + _heigth > anotherRect.LeftBottCornerCoords.Y + anotherRect.Heigth ? _leftBottCornerCoords.Y + _heigth : anotherRect.LeftBottCornerCoords.X + anotherRect.Heigth;
			
			//  transforming coords into Width and Heigth and return Rectangle
            return new Rectangle(new Point(leftSide, bottSide), rightSide - leftSide, topSide - bottSide);
        }

        public Rectangle GetCrossedRect(Rectangle second)
        {
			//  Return rectangle that is the result of crossing of two other rectangles
            Rectangle rect = getCrossedRect(this, second);
            if (rect == null)
            {
                rect = getCrossedRect(second, this);
            }
            return rect;
        }

        public bool IsPointBelongsToRectangle(Point point)
        {
            if (_leftBottCornerCoords.X <= point.X && _leftBottCornerCoords.X + _width >= point.X)
            {
                if (_leftBottCornerCoords.Y <= point.Y && _leftBottCornerCoords.Y + _heigth >= point.Y)
                {
                    return true;
                }
            }
            return false;
        }

        public void ChangeWidth(double newWidth)
        {
            _width = newWidth;
        }

        public void ChangeHeigth(double newHeigth)
        {
            _heigth = newHeigth;
        }

        public void MoveInYAxeWithStep(double step)
        {
            _leftBottCornerCoords.SetY(_leftBottCornerCoords.Y + step);
        }

        public void MoveInXAxeWithStep(double step)
        {
            _leftBottCornerCoords.SetX(_leftBottCornerCoords.X + step);
        }

        public override string ToString()
        {
            return "Left bottom point: " + _leftBottCornerCoords + ",  Width = " + Width + ", Heigth = " + Heigth;
        }
		
		private Rectangle getCrossedRect(Rectangle first, Rectangle second)
        {
            
            Point point = new Point(second.LeftBottCornerCoords);
            List<Point> resPointList = new List<Point>();
            //  pointPosition help us to know which corners belong to other rect 1 - leftBott; 2 - rigthBott; 3 - rightTop; 4 - leftTop
            int[] pointPosition = new int[4];
            int index = 0;

            //  find points which belong to another rect
            if (first.IsPointBelongsToRectangle(point))
            {
                resPointList.Add(new Point(point));
                pointPosition[index] = 1;
                index++;
            }

            point.SetX(point.X + second.Width);
            if (first.IsPointBelongsToRectangle(point))
            {
                resPointList.Add(new Point(point));
                pointPosition[index] = 2;
                index++;
            }

            point.SetY(point.Y + second.Heigth);
            if (first.IsPointBelongsToRectangle(point))
            {
                resPointList.Add(new Point(point));
                pointPosition[index] = 3;
                index++;
            }

            point.SetX(point.X - second.Width);
            if (first.IsPointBelongsToRectangle(point))
            {
                resPointList.Add(new Point(point));
                pointPosition[index] = 4;
                index++;
            }
            // end finding points

            // analysing points
            if (index == 4)
            {
                return second;
            }

            if (index == 2)
            {
                if (pointPosition[0] == 1 && pointPosition[1] == 2)
                {
                    return new Rectangle(resPointList[0], second.Width,
                                            first.Heigth - (second.LeftBottCornerCoords.Y - first.LeftBottCornerCoords.Y));
                }

                if (pointPosition[0] == 2 && pointPosition[1] == 3)
                {
                    Point lbCornerCoord = new Point(first.LeftBottCornerCoords.X, second.LeftBottCornerCoords.Y);
                    return new Rectangle(lbCornerCoord, second.LeftBottCornerCoords.X + second.Width - first.LeftBottCornerCoords.X,
                                            second.Heigth);
                }

                if (pointPosition[0] == 3 && pointPosition[1] == 4)
                {
                    Point lbCornerCoord = new Point(second.LeftBottCornerCoords.X, first.LeftBottCornerCoords.Y);
                    return new Rectangle(lbCornerCoord, second.Width,
                                            second.LeftBottCornerCoords.Y + second.Heigth - first.LeftBottCornerCoords.Y);
                }

                if (pointPosition[0] == 1 && pointPosition[1] == 4)
                {
                    return new Rectangle(resPointList[0], first.Width - (second.LeftBottCornerCoords.X - first.LeftBottCornerCoords.X),
                                            second.Heigth);
                }
            }

            if (index == 1)
            {
                if (pointPosition[0] == 1)
                {
                    return new Rectangle(resPointList[0], first.LeftBottCornerCoords.X + first.Width - resPointList[0].X,
                                             first.LeftBottCornerCoords.Y + first.Heigth - resPointList[0].Y);

                }

                if (pointPosition[0] == 2)
                {
                    Point lbCornerCoord = new Point(second.LeftBottCornerCoords.X + first.LeftBottCornerCoords.X - second.LeftBottCornerCoords.X, 
                                                    resPointList[0].Y);
                    return new Rectangle(lbCornerCoord, resPointList[0].X - lbCornerCoord.X,
                                             first.Heigth - lbCornerCoord.Y - first.LeftBottCornerCoords.Y);

                }

                if (pointPosition[0] == 3)
                {
                    Point lbCornerCoord = first.LeftBottCornerCoords;
                    return new Rectangle(lbCornerCoord, resPointList[0].X - lbCornerCoord.X,
                                             resPointList[0].Y - lbCornerCoord.Y);

                }

                if (pointPosition[0] == 4)
                {
                    Point lbCornerCoord = new Point(resPointList[0].X, first.LeftBottCornerCoords.Y);
                    return new Rectangle(lbCornerCoord, first.LeftBottCornerCoords.X + first.Width - lbCornerCoord.X,
                                             resPointList[0].Y - lbCornerCoord.Y);

                }
            }

            // end analysing
            return null;
        }

    }

    class MainClass
    {
        static void Main()
        {
            Rectangle firstRect = new Rectangle(new Point(2, 2), 2, 2);
            Rectangle secondRect = new Rectangle(new Point(0, 0), 3, 3);
            Console.WriteLine(firstRect);
            Console.WriteLine(secondRect);
            Console.WriteLine();


            Console.WriteLine("First rectangle cross second rectangle and make this rectangle:");
            Rectangle thirdRect = firstRect.GetCrossedRect(secondRect);
            Console.WriteLine(thirdRect);
            Console.WriteLine();
            Console.WriteLine("Also this rectangles make such rectangle as:");
            Rectangle fourthRect = firstRect.GetSmallestRectCombinedWithAnotherRect(secondRect);
            Console.WriteLine(fourthRect);
            Console.WriteLine();

			Console.WriteLine("Let`s move third rect");
			
            Console.WriteLine("Step in X axe = 5");
            double xStep = 5;
            
            Console.WriteLine("Step in Y axe = 0");
            double yStep = 0;
           
            Console.WriteLine("New width = 3");
            double width = 3;
           
            Console.WriteLine("New heigth = 10");
            double heigth = 10;
            			
            thirdRect.MoveInXAxeWithStep(xStep);
            thirdRect.MoveInYAxeWithStep(yStep);
            thirdRect.ChangeHeigth(heigth);
            thirdRect.ChangeWidth(width);
            Console.WriteLine(thirdRect);
        }
    }
}