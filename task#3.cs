using System;

namespace EpamItLab.Hometasks
{
    class Vector
    {
        private int[] _coordinatesMass;
        private int _leftBound;
        private int _rightBound;

		public Vector(int leftBound, int rightBound)
        {
            if (leftBound > rightBound)
            {
                int tempInt = leftBound;
                leftBound = rightBound;
                rightBound = tempInt;
            }

            _coordinatesMass = new int[rightBound + 1];
            _leftBound = leftBound;
            _rightBound = rightBound;
        }
		
        public int LeftBound
        {
            get
            {
                return _leftBound;
            }
            private set
            {
                _leftBound = value;
            }
        }

        public int RightBound
        {
            get
            {
                return _rightBound;
            }
            private set
            {
                _rightBound = value;
            }
        }

        public void Show()
        {
            for (int i = LeftBound; i <= RightBound; i++)
            {
                Console.Write("\t{0}", _coordinatesMass[i]);
            }
            Console.WriteLine();
        }
	   
		public int GetLength()
		{
			return _rightBound - _leftBound + 1;
		}
		
        public int this[int index]
        {
            get
            {
                if (index >= _leftBound && index <= _rightBound)
                {
                    return _coordinatesMass[index];
                }
                else
                {
                    Console.WriteLine("Out of range, returning null");
                    throw new Exception("Out of range exception");
                }
            }

            set
            {
                if (index >= _leftBound && index <= _rightBound)
                {
                    _coordinatesMass[index] = value;
                }
                else
                {
                    Console.WriteLine("Out of range");
                    throw new Exception("Out of range exception");
                }
            }
        }

        public static Vector operator +(Vector first, Vector second)
        {
            if (first.LeftBound == second.LeftBound && first.RightBound == second.RightBound)
            {
                Vector resultVector = new Vector(first.LeftBound, first.RightBound);
                for (int i = first.LeftBound; i <= first.RightBound; i++)
                {
                    resultVector[i] = first[i] + second[i];
                }
                return resultVector;
            }
            else
            {
                throw new Exception("Indexes intervals are different");
            }
        }

        public static Vector operator -(Vector first, Vector second)
        {
            return first + second * (-1);
        }

        public static Vector operator *(Vector justVector, int scalar)
        {

            Vector resultVector = new Vector(justVector.LeftBound, justVector.RightBound);
            for (int i = justVector.LeftBound; i <= justVector.RightBound; i++)
            {
                resultVector[i] = justVector[i] * scalar;
            }
            return resultVector;
        }

        public static Vector operator *(int scalar, Vector justVector)
        {
            return justVector * scalar;
        }
      
        public static bool operator ==(Vector first, Vector second)
        {
			// compare Vector components
			if (first.LeftBound == second.LeftBound && first.RightBound == second.RightBound)
            {
                for (int i = first.LeftBound; i <= first.RightBound; i++)
                {
                    if (first[i] != second[i])
					{
						return false;
					}
                }
                return true;
            }
            else
            {
                throw new Exception("Indexes intervals are different");
            }
        }

        public static bool operator !=(Vector first, Vector second)
        {
            return !(first == second);
        }    
    }


    class JustMainClass
    {
        static void Main()
        {
            Vector firstVector = new Vector(10, 15);
            Vector secondVector = new Vector(10, 15);

            try
            {
                for (int i = 10; i < 15; i++)
                {
                    firstVector[i] = i;
                    secondVector[i] = 15 - i;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Console.WriteLine("a =");
            firstVector.Show();
            Console.WriteLine("b =");
            secondVector.Show();

            Vector thirdVector = firstVector - secondVector;
            Console.WriteLine("a - b =");
            thirdVector.Show();

            Console.WriteLine("(a - b) * 3 =");
            thirdVector = thirdVector * 3;
            thirdVector.Show();
			
			string trueFalseStr = firstVector == secondVector ? "true" : "false";
			Console.Write("a == b : {0}",trueFalseStr);
        }
    }
}